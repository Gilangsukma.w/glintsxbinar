function CubeVolume(panjang, lebar, tinggi) {
  const formula = panjang * lebar * tinggi;
  return formula;
}

function TubeVolume(diameter, tinggi) {
  r = diameter / 2;
  const formula = Math.PI * r * r * tinggi;
  return formula;
}

let cube = CubeVolume(2, 2, 2);
let tube = TubeVolume(40, 20);

console.log("Cube volume : ", cube, "|", "Tube volume : ", tube);
