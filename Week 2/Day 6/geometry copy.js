// Import readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let isCubeCalculated = false;
let isTubeCalculated = false;

function CubeVolume(length, width, height) {
  const formula = length * width * height;
  isCubeCalculated = true;
  return formula;
}

function TubeVolume(diameter, height) {
  r = diameter / 2;
  const formula = Math.PI * r * r * height;
  isTubeCalculated = true;
  return formula;
}

// Function for calculate Cube
function calculateCube() {
  rl.question(`Enter the LENGTH of the Cube: `, (length) => {
    rl.question("Enter the WIDTH of the Cube: ", (width) => {
      rl.question("Enter the HEIGHT of the Cube: ", (height) => {
        if (!isNaN(length) && !isNaN(width) && !isNaN(height)) {
          const value = CubeVolume(length, width, height);
          console.log(value);
        } else {
          console.log(`Length, Width and Height must be a number\n`);
          calculateCube();
        }
      });
    });
  });
}

// Function for calculate Tube
function calculateTube() {
  rl.question(`Enter the DIAMETER of the Tube: `, (diameter) => {
    rl.question("Enter the HEIGHT of the Tube: ", (height) => {
      if (!isNaN(diameter) && !isNaN(height)) {
        const value = TubeVolume(diameter, height);
        console.log(value);
      } else {
        console.log(`Diameter and Height must be a number\n`);
        calculateTube();
      }
    });
  });
}

//Inputing value
function welcomeinput() {
  console.log("====================================");
  console.log("|| Welcome to this simple program ||");
  console.log("====================================");
  console.log("1: Calculate the volume of Cube");
  console.log("2: Calculate the volume of Tube");
  rl.question(
    "Which one do you want to calculate first? : ",
    function (choose) {
      if (!isNaN(choose)) {
        calculateCube();
        if (isCubeCalculated) {
          calculateTube();
          rl.close();
        }
        if (choose === "1") {
        } else if ((choose >= "3") | (choose <= "0")) {
          console.log(`Please Choose 1 or 2\n`);
          welcomeinput();
        } else {
          calculateTube();
          if (isTubeCalculated) {
            calculateCube();
            rl.close();
          }
        }
      } else {
        console.log(`value must be a number\n`);
        input();
      }
    }
  );
}

welcomeinput(); // Call input Function
