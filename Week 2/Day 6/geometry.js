// Import readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function CubeVolume(length, width, height) {
  const formula = length * width * height;
  return formula;
}

function TubeVolume(diameter, height) {
  r = diameter / 2;
  const formula = Math.PI * r * r * height;
  return formula;
}

// Function for calculate Cube
function calculateCube() {
  console.log("+==============================================+");
  console.log("+                   Cube Volume                +");
  console.log("+==============================================+");
  rl.question(`Enter the LENGTH of the Cube: `, (length) => {
    rl.question("Enter the WIDTH of the Cube: ", (width) => {
      rl.question("Enter the HEIGHT of the Cube: ", (height) => {
        if (!isNaN(length) && !isNaN(width) && !isNaN(height)) {
          const value = CubeVolume(length, width, height);
          console.log("Cube volume is : ", value);
          calculateTube();
        } else {
          console.log(`Length, Width and Height must be a number\n`);
          calculateCube();
        }
      });
    });
  });
}

// Function for calculate Tube
function calculateTube() {
  console.log("+==============================================+");
  console.log("+                   Tube Volume                +");
  console.log("+==============================================+");
  rl.question(`Enter the DIAMETER of the Tube: `, (diameter) => {
    rl.question("Enter the HEIGHT of the Tube: ", (height) => {
      if (!isNaN(diameter) && !isNaN(height)) {
        const value = TubeVolume(diameter, height);
        console.log("Tube volume is : ", value);
        rl.close();
      } else {
        console.log(`Diameter and Height must be a number\n`);
        calculateTube();
      }
    });
  });
}

calculateCube(); // Call input Function
