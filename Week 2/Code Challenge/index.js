const data = require("./lib/arrayFactory.js");
const test = require("./lib/test.js");

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter((i) => typeof i === "number");
}

// Should return array
function sortAscending(data) {
  // Code Here
  let newData = clean(data);

  for (let i = 0; i < newData.length; i++) {
    for (let j = 0; j < newData.length; j++) {
      if (newData[j] > newData[j + 1]) {
        let temp = newData[j];
        newData[j] = newData[j + 1];
        newData[j + 1] = temp;
      }
    }
  }
  return newData;
}

// Should return array
function sortDecending(data) {
  let newData = clean(data);

  for (let i = 0; i < newData.length; i++) {
    for (let j = 0; j < newData.length; j++) {
      if (newData[j] < newData[j + 1]) {
        let temp = newData[j];
        newData[j] = newData[j + 1];
        newData[j + 1] = temp;
      }
    }
  }
  return newData;
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
