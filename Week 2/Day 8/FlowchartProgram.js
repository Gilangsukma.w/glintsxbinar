let patientRegistered = false;
let queuenumber = 0;
let needFollowUp = true;
let needMedication = true;

function patientArrives() {
  console.log("Patient arrives");
  isPatientRegistered();
}

function isPatientRegistered() {
  console.log("Checking if patient already registered");
  //check if the patient already register
  if (patientRegistered) {
    //if yes then take queue number
    console.log("Patient already registered");
    takeQueueNumber();
  } else {
    //if no then patient need to register first and then go to next step
    console.log("Patient not registered");
    console.log("Registering patient...");
    setTimeout(() => {
      patientRegistered = true;
      console.log("Patient registered");
      takeQueueNumber();
    }, 3000);
  }
}

function takeQueueNumber() {
  if (isPatientRegistered) {
    queuenumber = Math.floor(Math.random() * (10 - 1)) + 1;
    console.log("Taking queue number : ", queuenumber);
    waitingForCalled();
  }
}

function waitingForCalled() {
  console.log("Waiting in lobby");
  for (let i = 1; i < 10; i++) {
    console.log(`Queue number ${i} is called`);
    if (i == queuenumber) {
      console.log("Patient called and go to doctor room");
      doctorCheckup();
      break;
    }
  }
}

function doctorCheckup() {
  console.log("doctor Checkup");
  patientNeedFollowUp();
}

function patientNeedFollowUp() {
  if (needFollowUp) {
    console.log("Patient need follow up");
    setTimeout(() => {
      console.log("Arrange next appointment");
      patientNeedMedication();
    }, 2000);
  } else {
    console.log("No need to followup check");
    patientNeedMedication();
  }
}

function patientNeedMedication() {
  if (needMedication) {
    console.log("Patient need medication");
    setTimeout(() => {
      console.log("Take medicine from pharmacy");
      finish();
    }, 2000);
  } else {
    console.log("No need for medication");
    finish();
  }
}

function finish() {
  console.log("Make payment");
  setTimeout(() => {
    console.log("Finish");
  }, Math.round(Math.random() * 3000));
}

function start() {
  patientArrives();
}

start();
