const url = require("../url");
const axios = require("axios");
//Fetch API with Axios

//====================================================
//                     Request
//====================================================

//get exercise list
const AxiosGetExercise = async () => {
  axios
    .get(url[0])
    .then((res) => {
      if (res.data.length > 0) {
        const response = res.data;
        console.log(response);
      }
    })
    .catch((err) => {
      const resError = { message: "error on fetch the API" };
      return resError;
    });
};

//get users list
const AxiosGetUser = async () => {
  axios
    .get(url[1])
    .then((res) => {
      if (res.data.length > 0) {
        const response = res.data;
        console.log(response);
      }
    })
    .catch((err) => {
      const resError = { message: "error on fetch the API" };
      return resError;
    });
};

//add Exercise
const AxiosAddExercise = async (data) => {
  axios
    .post(url[2], data)
    .then((res) => {
      const response = res.data;
      console.log("====================");
      console.log(`|  ${response}  |`);
      console.log("====================");
      return response;
    })
    .catch((err) => {
      const resError = { message: "error on fetch the API" };
      return resError;
    });
};

module.exports = { AxiosGetUser, AxiosGetExercise, AxiosAddExercise };
