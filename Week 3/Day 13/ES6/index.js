const url = require("./url");
const { AxiosGetExercise, AxiosAddExercise, AxiosGetUser } = require("./Axios");
const { FetchAddExercise, FetchGetExercise, FetchGetUser } = require("./Fetch");

//====================================================
//                     Parameter
//====================================================
//this variable valueExercise required for adding exercise request, you can edit the value if you want :)
var date = new Date();
var now = date.toISOString();
const valueExercise = {
  username: "username here",
  description: "your description here",
  duration: 55, //hours
  date: now,
};

const Axios = async () => {
  console.log("               Fetching with Axios");
  console.log("====================================================");
  console.log("You can see the GUI Version here :");
  console.log("https://simple-crud-exercise.netlify.app/");
  console.log("====================================================");

  //====================================================
  //                     TryCatch
  //====================================================

  try {
    AxiosGetExercise();
    // AxiosGetUser();
    // AxiosAddExercise(valueExercise);
  } catch (error) {
    console.log(error);
  }
};

//fetch API
const Fetch = async () => {
  console.log("              Fetching with Fetch-node");
  console.log("====================================================");
  console.log("You can see the GUI Version here : ");
  console.log("https://simple-crud-exercise.netlify.app/");
  console.log("====================================================");
  //====================================================
  //                     TryCatch
  //====================================================

  try {
    // FetchGetExercise();
    FetchGetUser();
    // FetchAddExercise(valueExercise);
  } catch (error) {
    console.log(error);
  }
};

Axios();
// Fetch();
