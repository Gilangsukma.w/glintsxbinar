const fetch = require("node-fetch");

//Fetch API with node-fetch

//====================================================
//                     Request
//====================================================

const url = require("../url");

const FetchGetExercise = async () => {
  try {
    fetch(url[0])
      .then((res) => res.json())
      .then((json) => {
        console.log(json);
      })
      .catch((err) => {
        console.log({ message: "error on fetch the API" });
      });
  } catch (error) {
    console.log(error);
  }
};

const FetchGetUser = async () => {
  try {
    fetch(url[1])
      .then((res) => res.json())
      .then((json) => {
        console.log(json);
      })
      .catch((err) => {
        console.log({ message: "error on fetch the API" });
      });
  } catch (error) {
    console.log(error);
  }
};

const FetchAddExercise = async (data) => {
  try {
    fetch(url[2])
      .then((res) => res.json())
      .then((json) => {
        console.log("====================");
        console.log(`|  ${json}  |`);
        console.log("====================");
      })
      .catch((err) => {
        console.log({ message: "error on fetch the API" });
      });
  } catch (error) {
    console.log(error);
  }
};

module.exports = { FetchGetExercise, FetchGetUser, FetchAddExercise };
