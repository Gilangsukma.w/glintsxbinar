const express = require("express");
const router = express.Router();

const {
  getAllStudents,
  getSpecifyStudents,
  addStudents,
  editStudent,
  deleteStudent,
} = require("../controller/dbController");

router.get("/", getAllStudents);

router.get("/id/:id", getSpecifyStudents);

router.post("/add", addStudents);

router.put("/edit/:id", editStudent);

router.delete("/delete/:id", deleteStudent);

module.exports = router;
