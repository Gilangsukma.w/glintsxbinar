const Vehicle = require("./vehicle");

class Airplane extends Vehicle {
  constructor(name, type, broken) {
    super(`${name}${type}${broken}`);
    this.name = name;
    this.type = type;
    this.broken = broken;
  }
}

module.exports = Airplane;
