class Vehicle {
  constructor(name) {
    this.name = name;
  }

  static staticFunction = "this is static function";

  description() {
    if (this.type === "land") {
      return `${this.name} is vehicle!, this is ${
        this.type
      } vehicle,and it has ${this.wheel} wheels, ${this.isBroken()}`;
    } else {
      return `${this.name} is vehicle!, this is ${
        this.type
      } vehicle, and it can fly!, ${this.isBroken()}`;
    }
  }

  isBroken() {
    if (this.broken) {
      return `${this.name} is broken`;
    } else {
      return `${this.name} is not broken`;
    }
  }
}

Vehicle.location = function (data) {
  if (data === "land") {
    return `you can find this vehicle on the street`;
  } else {
    return `you can find this vehicle on the airport`;
  }
};

module.exports = Vehicle;
