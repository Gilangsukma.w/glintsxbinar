const Vehicle = require("./vehicle");
const Land = require("./Land");
const Airplane = require("./Airplane");

console.log(Vehicle.staticFunction);

// const data = ["Cars", 4, "land", false];
const data = ["chopper", "airplane", false];

// let vehicle1 = new Land(data[0], data[1], data[2]);
let vehicle1 = new Airplane(data[0], data[1], data[2]);

console.log(vehicle1.description());
console.log(Vehicle.location(data[1]));
