const Vehicle = require("./vehicle");

class Land extends Vehicle {
  constructor(name, wheel, type, broken) {
    super(`${name}${wheel}${type}${broken}`);
    this.name = name;
    this.wheel = wheel;
    this.type = type;
    this.broken = broken;
  }
}

module.exports = Land;
