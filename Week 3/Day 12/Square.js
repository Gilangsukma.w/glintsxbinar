const TwoDimention = require("./TwoDimention");

class Square extends TwoDimention {
  static count = 0;
  constructor(width, length) {
    super("Square");
    this.width = width;
    this.length = length;
    Square.count++;
  }

  whoAmI() {
    super.whoAmI();
    console.log(`I'm ${this.name} ${this.count}`);
  }

  calculateCircumference(name) {
    super.calculateCircumference();
    const formula = 2 * (this.width + this.length);
    return `${name}, ${formula}`;
  }

  calculateArea(name) {
    super.calculateArea();
    const formula = this.width * this.length;
    return `${name}, ${formula}`;
  }
}

module.exports = Square;
