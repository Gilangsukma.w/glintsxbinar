const Geometry = require("./Geometry");

class TwoDimention extends Geometry {
  count = 0;
  constructor(name) {
    super(name, "Two Dimention");
    TwoDimention.count++;
  }

  whoAmI() {
    console.log(`Im ${this.type}`);
  }

  calculateCircumference() {
    return console.log("====== Calculating Circumference ======");
  }

  calculateArea() {
    return console.log("====== Calculating Area ======");
  }
}

module.exports = TwoDimention;
