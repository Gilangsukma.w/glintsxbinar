class Geometry {
  static count = 0;

  constructor(name, type) {
    this.name = name;
    this.type = type;
    Geometry.count++;
  }

  whoAmI() {
    console.log("Who am I");
  }
}

module.exports = Geometry;
